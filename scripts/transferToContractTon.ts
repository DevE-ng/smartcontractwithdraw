import {Address, toNano} from '@ton/core';
import { SendTon } from '../wrappers/SendTon';
import { NetworkProvider } from '@ton/blueprint';
import {Blockchain} from "@ton/sandbox";

export async function run(provider: NetworkProvider) {
    //const sendTon = provider.open(await SendTon.fromInit());

    // @ts-ignore
    const sendTonAddress = Address.parse("EQC2mSEQDTbje0EZzOUur6KmZu_Bvr0jWYGYZWj0VksjUd9q");//insert address of smart contract node
    const sendTon = provider.open(await SendTon.fromAddress(sendTonAddress));

    await sendTon.send(
        provider.sender(),
        {
            value: toNano('2'),
        },
        null
    );



    // run methods on `sendTon`
}
