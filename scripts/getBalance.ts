import {Address, fromNano, toNano} from '@ton/core';
import { SendTon } from '../wrappers/SendTon';
import { NetworkProvider } from '@ton/blueprint';

export async function run(provider: NetworkProvider) {
    const sendTonAddress = Address.parse("EQC2mSEQDTbje0EZzOUur6KmZu_Bvr0jWYGYZWj0VksjUd9q");
    const sendTon = provider.open(await SendTon.fromAddress(sendTonAddress));

    const balance = await sendTon.getBalance()

    console.log(fromNano(balance));

    // run methods on `sendTon`
}
